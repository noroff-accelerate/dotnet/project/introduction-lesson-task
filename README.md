# Book Repository Console Application

This is a simple C# console application that demonstrates the use of generic classes, control structures, and basic user interaction. The application manages a collection of books, allowing users to add, remove, and view books.

## Features

- Add books to the repository with unique ISBN numbers.
- Remove books from the repository using ISBN numbers.
- Display all books in the repository.
- Basic validation to prevent duplicate ISBN entries.

## How to Use

1. Run the application.
2. Follow the on-screen prompts:
   - Enter `Add` to add a new book.
   - Enter `Remove` to remove a book.
   - Enter `Display` to view all books.
   - Enter `Exit` to terminate the application.
3. For adding a book, enter the title, author, ISBN, and price as prompted.
4. For removing a book, enter the book's ISBN.

## Installation

Clone this repository or download the source code. You need a C# compiler or an IDE like Visual Studio to compile and run the application.

## Contributing

Feel free to fork the repository and submit pull requests. You can also open issues for bugs you've found or features you think would be useful.

## License

MIT License

Copyright (c) 2023 Noroff Accelerate.

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER I
