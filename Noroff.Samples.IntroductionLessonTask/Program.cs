﻿namespace Noroff.Samples.IntroductionLessonTask
{
    internal class Program
    {
        static void Main(string[] args)
        {
            var bookRepository = new Repository<Book>();
            string userInput;

            while (true)
            {
                Console.WriteLine("Enter command (Add, Remove, Display, Exit):");
                userInput = Console.ReadLine();

                if (userInput.Equals("Add", StringComparison.OrdinalIgnoreCase))
                {
                    Console.WriteLine("Enter book title:");
                    string title = Console.ReadLine();

                    Console.WriteLine("Enter book author:");
                    string author = Console.ReadLine();

                    Console.WriteLine("Enter book ISBN:");
                    string isbn = Console.ReadLine();

                    Console.WriteLine("Enter book price:");
                    decimal price = decimal.Parse(Console.ReadLine());

                    var newBook = new Book { Title = title, Author = author, ISBN = isbn, Price = price };

                    var existingBook = bookRepository.GetAll().FirstOrDefault(b => b.ISBN == isbn);
                    if (existingBook == null)
                    {
                        bookRepository.Add(newBook);
                    }
                    else
                    {
                        Console.WriteLine("A book with this ISBN already exists.");
                    }
                }
                else if (userInput.Equals("Remove", StringComparison.OrdinalIgnoreCase))
                {
                    Console.WriteLine("Enter the ISBN of the book to remove:");
                    var isbnToRemove = Console.ReadLine();
                    var bookToRemove = bookRepository.GetAll().FirstOrDefault(b => b.ISBN == isbnToRemove);

                    if (bookToRemove != null)
                    {
                        bookRepository.Remove(bookToRemove);
                    }
                    else
                    {
                        Console.WriteLine("Book not found.");
                    }
                }
                else if (userInput.Equals("Display", StringComparison.OrdinalIgnoreCase))
                {
                    foreach (var book in bookRepository.GetAll())
                    {
                        Console.WriteLine($"Title: {book.Title}, Author: {book.Author}, ISBN: {book.ISBN}, Price: {book.Price}");
                    }
                }
                else if (userInput.Equals("Exit", StringComparison.OrdinalIgnoreCase))
                {
                    break;
                }
            }
        }
    }

    public class Book
    {
        public string Title { get; set; }
        public string Author { get; set; }
        public string ISBN { get; set; }
        public decimal Price { get; set; }
    }


    public class Repository<T>
    {
        private List<T> items = new List<T>();

        public void Add(T item)
        {
            items.Add(item);
            Console.WriteLine("Item added successfully.");
        }

        public bool Remove(T item)
        {
            bool removed = items.Remove(item);
            if (removed)
            {
                Console.WriteLine("Item removed.");
            }
            else
            {
                Console.WriteLine("Item not found.");
            }
            return removed;
        }

        public List<T> GetAll()
        {
            return items;
        }
    }



}